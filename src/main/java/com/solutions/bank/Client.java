package com.solutions.bank;

record Client(
  String name,
  Deal deal,
  long money,
  long time
) {
}
