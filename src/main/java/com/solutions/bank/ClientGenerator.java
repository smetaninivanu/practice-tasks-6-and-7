package com.solutions.bank;

import lombok.SneakyThrows;

import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class ClientGenerator implements Runnable {

  private final List<BankOfficer> officers;

  private final Bank bank;

  private boolean isActive;

  private final Random random;

  private int clientNumber = 1;

  public ClientGenerator(List<BankOfficer> officers, Bank bank) {
    this.officers = officers;
    this.isActive = true;
    this.random = new Random();
    this.bank = bank;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  @SneakyThrows
  @Override
  public void run() {
    while (isActive) {
      BankOfficer bankOfficer = officers
        .stream()
        .min(Comparator.comparing(BankOfficer::getQueueSize))
        .orElse(officers.get(0));

      System.out.printf("Новый клиент проходит в очередь %d. Текущее количество клиентов в очередях: ", bankOfficer.getOfficerNumber());
      for (BankOfficer officer : officers) {
        System.out.print(officer.getQueueSize() + " ");
      }
      System.out.println();

      String name = "John  " + clientNumber++;
      Deal deal = random.nextBoolean() ? Deal.deposit : Deal.withdraw;
      long money = random.nextLong(bank.getCash() + bank.getCash() / 2);

      officers.get(bankOfficer.getOfficerNumber() - 1)
        .addClient(new Client(name, deal, money, Main.SERVICE_TIME * 1000 + random.nextLong(1000)));

      Thread.sleep(60 / Main.CLIENTS_PER_MINUTE * 1000 + random.nextLong(1000));
    }
  }
}
