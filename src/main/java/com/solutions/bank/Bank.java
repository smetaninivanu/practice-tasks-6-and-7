package com.solutions.bank;

public class Bank {
  private volatile long cash;

  public Bank(long cash) {
    this.cash = cash;
  }

  public long getCash() {
    return cash;
  }

  public synchronized void changeCash(Client client, int officerNumber) {
    if (client.deal().equals(Deal.deposit)) {
      cash += client.money();
      System.out.printf("Клиент %s положил на счёт банка %d$ через операциониста в окне %d\n",
        client.name(), client.money(), officerNumber);
    } else {
      if (cash - client.money() > 0) {
        cash -= client.money();
        System.out.printf("Клиент %s снял со счёта банка %d$ через операциониста в окне %d\n",
          client.name(), client.money(), officerNumber);
      } else {
        System.out.printf("Клиент %s не смог снять со счёта банка %d$ через операциониста в окне %d. В банке недостаточно средств\n",
          client.name(), client.money(), officerNumber);
      }
    }
  }
}

