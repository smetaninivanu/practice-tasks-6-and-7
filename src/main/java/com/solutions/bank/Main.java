package com.solutions.bank;


import java.util.List;
import java.util.stream.IntStream;

class Main {

  public static final int OFFICERS_COUNT = 8;

  // В секундах
  public static final int SERVICE_TIME = 9;

  public static final int CLIENTS_PER_MINUTE = 60;

  public static final long BANK_CASH = 10_000L;

  public static void loadBank() {
    Bank bank = new Bank(BANK_CASH);

    List<BankOfficer> officers = IntStream.range(1, OFFICERS_COUNT + 1).boxed()
      .map(t -> new BankOfficer(bank, t)).toList();

    officers.forEach(officer -> new Thread(officer).start());

    new Thread(new ClientGenerator(officers, bank)).start();
  }

  public static void main(String[] args) {
    loadBank();
  }
}
