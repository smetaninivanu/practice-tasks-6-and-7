package com.solutions.bank;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BankOfficer implements Runnable {

  private final Bank bank;

  private boolean isActive;

  private final int officerNumber;

  private final List<Client> clientList;

  public BankOfficer(Bank bank, int officerNumber) {
    this.clientList = Collections.synchronizedList(new ArrayList<>());
    this.bank = bank;
    this.officerNumber = officerNumber;
    this.isActive = true;
  }

  public int getQueueSize() {
    synchronized (clientList){
      return clientList.size();
    }
  }

  public int getOfficerNumber() {
    return officerNumber;
  }

  public void addClient(Client client) {
    synchronized (clientList){
      clientList.notify();
    }

    clientList.add(client);
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  @Override
  @SneakyThrows
  public void run() {
    while (isActive) {
      if (clientList.size() == 0) {
        synchronized (clientList){
          clientList.wait();
        }
        continue;
      }

      Client client = clientList.get(0);

      bank.changeCash(client, officerNumber);

      Thread.sleep(client.time());

      clientList.remove(0);
    }
  }
}
