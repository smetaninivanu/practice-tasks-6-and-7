package com.solutions.matrices;

import java.util.ArrayList;
import java.util.List;

public class MatrixParallelMultiplier {

  /**
   * Waits for all threads to die in this list and clears this list.
   *
   * @param threads list of threads
   */
  private static void waitAndClearThreadsList(List<Thread> threads) {
    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    threads.clear();
  }

  /**
   * Multiply specified matrices
   *
   * @param matrixFirst  double dimensional array of doubles
   * @param matrixSecond double dimensional array of doubles
   * @return result       result of multiplication
   */
  public static double[][] multiply(double[][] matrixFirst, double[][] matrixSecond) {

    if (matrixFirst[0].length != matrixSecond.length) {
      throw new IllegalMatricesException("The number of columns in matrixFirst is not equal the number of rows in matrixSecond");
    }

    int availableProcessors = Runtime.getRuntime().availableProcessors();
    double[][] result = new double[matrixFirst.length][matrixSecond[0].length];
    List<Thread> threads = new ArrayList<>();

    for (int i = 0; i < matrixFirst.length; i++) {
      if (threads.size() % availableProcessors == 0) {
        waitAndClearThreadsList(threads);
      }

      Thread thread = new Thread(new RowMultiplier(matrixFirst, matrixSecond, i, result));
      thread.start();

      threads.add(thread);
    }

    return result;
  }
}
