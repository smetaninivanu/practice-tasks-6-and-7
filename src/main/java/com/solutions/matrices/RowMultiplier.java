package com.solutions.matrices;

record RowMultiplier(
  double[][] matrixFirst,
  double[][] matrixSecond,
  int row,
  double[][] result
) implements Runnable {

  /**
   * Multiply specified row of two matrices.
   */
  @Override
  public void run() {
    for (int i = 0; i < matrixSecond[0].length; i++) {
      result[row][i] = 0;
      for (int j = 0; j < matrixFirst[row].length; j++) {
        result[row][i] += matrixFirst[row][j] * matrixSecond[j][i];
      }
    }
  }
}
