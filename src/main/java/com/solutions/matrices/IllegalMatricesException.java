package com.solutions.matrices;

public class IllegalMatricesException extends RuntimeException {
  public IllegalMatricesException(String message) {
    super(message);
  }
}
