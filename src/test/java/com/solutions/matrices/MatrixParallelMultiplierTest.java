package com.solutions.matrices;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MatrixParallelMultiplierTest {

  @Test
  void multiplyCorrectBehavior() {
    double[][] m1 = {{0, 1, 2}, {3, 4, 5}};
    double[][] m2 = {{3, 4, 5}, {0, 1, 2}, {0, 1, 2}};

    double[][] result = MatrixParallelMultiplier.multiply(m1, m2);

    assertArrayEquals(
      new double[][]{{0, 3, 6}, {9, 21, 33}},
      result
    );
  }

  @Test
  void multiplyIncorrectBehavior() {
    double[][] m1 = new double[][]{{0, 1, 2}, {3, 4, 5}};
    double[][] m2 = new double[][]{{3, 4, 5}, {0, 1, 2}};

    assertThrows(
      IllegalMatricesException.class,
      () -> MatrixParallelMultiplier.multiply(m1, m2)
    );
  }
}
