package com.solutions.matrices;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RowMultiplierTest {

  // Не рассматривается тест при неверных входных данных,
  // потому что класс RowMultiplier не используется нигде,
  // кроме как в контексте MatrixParallelMultiplier
  
  @Test
  void run() {
    double[][] m1 = {{0, 1, 2}, {3, 4, 5}};
    double[][] m2 = {{3, 4, 5}, {0, 1, 2}, {0, 1, 2}};

    double[][] result = new double[m1.length][m2[0].length];

    RowMultiplier rowMultiplierFirst = new RowMultiplier(m1, m2, 0, result);
    rowMultiplierFirst.run();

    assertArrayEquals(
      new double[][]{{0, 3, 6}, {0, 0, 0}},
      result
    );

    RowMultiplier rowMultiplierSecond = new RowMultiplier(m1, m2, 1, result);
    rowMultiplierSecond.run();

    assertArrayEquals(
      new double[][]{{0, 3, 6}, {9, 21, 33}},
      result
    );
  }
}
